"use strict"

const mongoose = require('mongoose');

module.exports = {
    initMongoose: initMongoose
}

function initMongoose() {
    mongoose.connect('mongodb://127.0.0.1:27017/nodejs', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })

    const db = mongoose.connection;

    db.on('error', function(err) {
        console.log('err', err)
    })

    db.once('open', function() {
        console.log('connected to mongodb')
    })
    process.on('SIGINT',cleanup); // Ctrl + c
    process.on('SIGTERM',cleanup); // TERMINATE PROCESS
    process.on('SIGHUP',cleanup); // TERMINAL
}

function cleanup() {
    console.log('cleanup');
    mongoose.connection.close(function() {
        process.exit()
    })
}
