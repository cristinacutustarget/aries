"use strict"

const User = require('../models/users');
module.exports = {
    mid1: mid1,
    mid2: mid2,
    mid3: mid3,
    createUser: createUser,
    getUsers: getUsers,
    getUsersById: getUsersById,
    deleteUserById: deleteUserById,
    updateUser: updateUser,
}

function updateUser(req, res, next) {
    const {userId} = req.params;

    User.findOneAndUpdate({_id: userId}, req.body, function(err, result) {
        if(err) {
            return next(err)
        }
        req.resources.users = result;
        return next();
    })
}

function deleteUserById(req, res, next) {
    const {userId} = req.params;

    const params = {
        _id: userId
    }

    User.find(
        {
            _id: userId,
        }
    )
    User.deleteOne({_id: userId}, function(err, result) {
        if(err) {
            return next(err)
        }
        next();
    })
}

function getUsersById(req, res, next) {
    console.log('params', req.params);
    const {userId} = req.params;

    User.findById({_id: userId}, function(err, result) {
        if(err) {
            return next(err)
        }

        req.resources.users = result;
        return next();
    })
}

function getUsers(req, res, next) {
    User.find(function (err, result) {
        if (err) {
            return next(err)
        }
        req.resources.users = result;
        return next();
    })
}

function createUser(req, res, next) {
    const user = new User(req.body);
    user.save(function(err, result) {
        if(err) {
            console.log('status code', err)
            err.statusCode = 401;
            return next(err)
        }
        req.resources.users = result;
        return next();
    })
}

function mid2(req, res, next) {
    console.log('users route get 2 mid');
    next();
}

function mid3(req, res, next) {
    console.log('users route get 3 mid');
    return res.send('message mid3');
}

function mid1(req, res, next) {
    console.log('users route get 1 mid', req.body);
    next();
}

