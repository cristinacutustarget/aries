"use strict"

const Book = require('../models/books');
module.exports = {
    createBook: createBook,
    getBooks: getBooks,
    getBooksById: getBooksById,
    deleteBookById: deleteBookById,
}

function deleteBookById(req, res, next) {
    const {bookId} = req.params;

    const params = {
        _id: bookId
    }

    Book.deleteOne({_id: bookId}, function(err, result) {
        if(err) {
            return res.status(404).json(err)
        }

        next();
    })
}

function getBooksById(req, res, next) {
    const {bookId} = req.params;

    Book.findById({_id: bookId}, function(err, result) {
        if(err) {
            return res.status(404).json(err)
        }

        req.resources.books = result;
        return next();
    })
}

function getBooks(req, res, next) {
    console.log('getBooks');
    Book
        .find()
        .populate('userId', 'email name')
        .exec(function(err, result) {
            if (err) {
                console.log('err', err);
                return res.status(404).json(err)
            }
            console.log('res', result);
            req.resources.books = result;
            next();
        })
}

function createBook(req, res, next) {
    const book = new Book(req.body);

    book.save(function(err, result) {
        if(err) {
            return res.status(404).json(err);
        }
        req.resources.books = result;
        return next();
    })
}
