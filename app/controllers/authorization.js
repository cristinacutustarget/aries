"use strict"

module.exports = {
    isAdmin: isAdmin
}

const userIsAdmin = true;

function isAdmin(req, res, next) {
    if (userIsAdmin) {
        next();
    } else {
        res.send('User is not authorized');
    }
}
