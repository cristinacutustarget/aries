"use strict"

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

const { userModel, bookModel } = require('../constants');

const bookSchema = new Schema({
    createdAt: Number,
    updatedAt: Number,
    title: {
        type: String,
        required: true,
        unique: false
    },
    author: {
        type: String,
        required: true,
        unique: false
    },
    year: {
        type: Number,
        required: true,
        unique: false
    },
    userId: {
        type: ObjectId,
        ref: userModel,
        required: true
    },
    reviews: [
        {
            description: {
                type: String,
            },
            rate: {
                type: Number
            }
        }
    ]
}, { timestamps: { currentTime: () => new Date().getTime() } })

module.exports = mongoose.model(bookModel, bookSchema, 'books');
