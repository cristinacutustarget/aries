"use strict"

const express = require('express')
const router = express.Router()
const bookCtrl = require('../controllers/books')
const helperCtrl = require('../helpers');

console.log('bookCtrl', bookCtrl);

router.get('/books',
    bookCtrl.getBooks,
    helperCtrl.responseToJson('books')
)

router.get('/books/:bookId',
    bookCtrl.getBooksById,
    helperCtrl.responseToJson('books')
)

router.post('/books',
    bookCtrl.createBook,
    helperCtrl.responseToJson('books')
)

router.delete('/books/:bookId',
    bookCtrl.getBooksById,
    bookCtrl.deleteBookById,
    helperCtrl.responseToJson('books')
)

module.exports = router;


