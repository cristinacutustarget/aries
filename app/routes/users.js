"use strict"

const express = require('express')
const router = express.Router()
const userCtrl = require('../controllers/users')
const authCtrl = require('../controllers/authorization');
const helperCtrl = require('../helpers');

router.get('/users',
    authCtrl.isAdmin,
    userCtrl.getUsers,
    // userCtrl.mid1,
    // userCtrl.mid2,
    // userCtrl.mid3,
    helperCtrl.responseToJson('users')
)

router.get('/users/:userId',
    userCtrl.getUsersById,
    helperCtrl.responseToJson('users')
)

router.post('/users',
    userCtrl.createUser,
    helperCtrl.responseToJson('users')
)

router.put('/users/:userId',
    userCtrl.updateUser,
    helperCtrl.responseToJson('users')
)

router.delete('/users/:userId',
    userCtrl.getUsersById,
    userCtrl.deleteUserById,
    helperCtrl.responseToJson('users')
)

module.exports = router;


