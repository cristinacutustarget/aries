"use strict"
const express = require('express');
const app = express();
const config = require('./config/index');

// init configs
require('./config/express').initExpress(app);
require('./config/routes').initRoutes(app);
require('./config/mongoose').initMongoose(app);
// end init configs

require('./config/finalRoute').finalRoute(app);
require('./config/handlingError').handlingError(app);

app.listen(config.PORT, function() {
    console.log(`App is running on port ${config.PORT}`);
});



